
from os.path import abspath, dirname, exists, join
from enum import Enum
import re, sys, time

from lexer import Lexer
from ply import lex, yacc

import inspect

DEBUG = True

# When using something like pyinstaller, the __file__ attribute isn't actually
# set correctly, so the parse file isn't able to be saved anywhere sensible.
# In these cases, just use a temporary directory, it doesn't take too long to
# generate the tables anyways... 

if exists(dirname(__file__)):
    pickle_file = abspath(join(dirname(__file__), 'parsetab.dat'))
else:
    import tempfile
    fobj = tempfile.NamedTemporaryFile()
    pickle_file = fobj.name


if sys.version_info[0] < 3:
    def iteritems(d):
        return iter(d.iteritems())
else:
    def iteritems(d):
        return iter(d.items())



class HclParser(object):
    
    #
    # Tokens
    #
    
    tokens = (
        'BOOL',
        'FLOAT',
        'NUMBER',
        'COMMA', 'COMMAEND', 'IDENTIFIER', 'EQUAL', 'STRING', 'MINUS',
        'LEFTBRACE', 'RIGHTBRACE', 'LEFTBRACKET', 'RIGHTBRACKET', 'PERIOD',
        'EPLUS', 'EMINUS', 'NL'
    )
    
    #
    # Yacc parser section
    #
    def objectlist_flat(self, lt, replace):
        '''
            Similar to the dict constructor, but handles dups
            
            HCL is unclear on what one should do when duplicate keys are
            encountered. These comments aren't clear either:
            
            from decoder.go: if we're at the root or we're directly within
                             a list, decode into dicts, otherwise lists
                
            from object.go: there's a flattened list structure
        '''
        d = {}
        
        for k,v in lt:
            if k in d.keys() and not replace:
                if type(d[k]) is list:
                    d[k].append(v)
                else:
                    d[k] = [d[k],v]
            else:
                if isinstance(v, dict):
                    dd = d.setdefault(k, {})
                    for kk, vv in iteritems(v):
                        if type(dd) == list:
                            dd.append({kk: vv})
                        elif kk in dd.keys():
                            if hasattr(vv, 'items'):
                                for k2, v2 in iteritems(vv):
                                    dd[kk][k2] = v2
                            else:
                                d[k] = [dd, {kk: vv}]
                        else:
                            dd[kk] = vv
                else:
                    d[k] = v

        return d
    
    def p_top(self, p):
        "top : objectlist"
        if DEBUG:
            self.print_p(p)
        p[0] = self.objectlist_flat(p[1],True)
    
    
    def p_objectlist_0(self, p):
        "objectlist : objectitem"
        if DEBUG:
            self.print_p(p)
        if not p[1]:
            return
        p[0] = [p[1]]
    
    def p_objectlist_1(self, p):
        "objectlist : objectlist objectitem"
        if DEBUG:
            self.print_p(p)
        if not p[1]:
            p[0] = [p[2]]
        elif not p[2]:
            p[0] = p[1]
        else:
            p[0] = p[1] + [p[2]]
        if DEBUG:
            self.print_p(p)

    def p_objectlist_2(self, p):
        "objectlist : objectlist COMMA objectitem"
        if DEBUG:
            self.print_p(p)
        p[0] = p[1] + [p[3]]
    
    def p_object_0(self, p):
        "object : LEFTBRACE objectlist RIGHTBRACE"
        if DEBUG:
            self.print_p(p)
        p[0] = self.objectlist_flat(p[2],False)
        
    def p_object_1(self, p):
        "object : LEFTBRACE objectlist COMMA RIGHTBRACE"
        if DEBUG:
            self.print_p(p)
        p[0] = self.objectlist_flat(p[2],False)

    def p_object_2(self, p):
        "object : LEFTBRACE RIGHTBRACE"
        if DEBUG:
            self.print_p(p)
        p[0] = {}
    
    def p_objectkey_0(self, p):
        '''
        objectkey : IDENTIFIER
                  | STRING
        '''
        if DEBUG:
            self.print_p(p)
        p[0] = p[1]
    
    def p_objectitem_0(self, p):
        '''
        objectitem : objectkey EQUAL number
                   | objectkey EQUAL BOOL
                   | objectkey EQUAL STRING
                   | objectkey EQUAL object
                   | objectkey EQUAL list
        '''
        line_no = p.lexer.lex.lineno - 2
        block = self.working_block_stack[-1]
        block.add_argument(line_no, LineType.BLOCK_ARGUMENT, {p[1] : p[3]})

        if DEBUG:
            self.print_p(p)
        p[0] = (p[1], p[3])
    
    def p_objectitem_1(self, p):
        "objectitem : NL"
        line_no = p.lexer.lex.lineno - 2

        try:
            block = self.working_block_stack[-1]
            block.add_argument(line_no, LineType.BLOCK_EMPTY_LINE, None)
        except IndexError:
            # We're not inside a block
            pass

        if DEBUG:
            self.print_p(p)

    def p_objectitem_2(self, p):
        "objectitem : block"
        if DEBUG:
            self.print_p(p)
        p[0] = p[1]
    
    
    def p_block_0(self, p):
        "block : blockId object"
        line_no = p.lexer.lex.lineno - 2
        # The end of a block.
        block = self.working_block_stack.pop()
        block.line_end = line_no
        self.block_stack.append(block)

        if DEBUG:
            self.print_p(p)
        p[0] = (p[1], p[2])
        
    def p_block_1(self, p):
        "block : blockId block"
        if DEBUG:
            self.print_p(p)
        p[0] = (p[1], {p[2][0]: p[2][1]})
        
    def p_blockId(self, p):
        '''
        blockId : IDENTIFIER
                | STRING
        '''
        line_no = p.lexer.lex.lineno - 1
        try:
            block = self.working_block_stack[-1]
        except IndexError:
            self.working_block_stack.append(Block(line_start=line_no))
            block = self.working_block_stack[-1]
            self.parent_block_indices.append(line_no)

        if block.line_start != line_no:
            # A new block.
            new_block = Block(line_start=line_no)
            new_block.add_block_identifier(p[1])
            self.working_block_stack.append(new_block)
            # Add the block to the parent's arguments
            block.add_argument(line_no, LineType.BLOCK_DEFINITION, new_block)
        else:
            block.add_block_identifier(p[1])

        if DEBUG:
            self.print_p(p)
        p[0] = p[1]
    
    
    def p_list_0(self, p):
        '''
        list : LEFTBRACKET listitems RIGHTBRACKET
             | LEFTBRACKET listitems COMMA RIGHTBRACKET
        '''
        if DEBUG:
            self.print_p(p)
        p[0] = p[2]
        
    def p_list_1(self, p):
        "list : LEFTBRACKET RIGHTBRACKET"
        if DEBUG:
            self.print_p(p)
        p[0] = []
        
        
    def p_listitems_0(self, p):
        "listitems : listitem"
        if DEBUG:
            self.print_p(p)
        p[0] = [p[1]]
        
    def p_listitems_1(self, p):
        "listitems : listitems COMMA listitem"
        if DEBUG:
            self.print_p(p)
        p[0] = p[1] + [p[3]]

    def p_listitem(self, p):
        '''
        listitem : number
                 | object
                 | STRING
        '''
        if DEBUG:
            self.print_p(p)
        p[0] = p[1]
        
    def p_number_0(self, p):
        "number : int"
        if DEBUG:
            self.print_p(p)
        p[0] = p[1]
        
    def p_number_1(self, p):
        "number : float"
        if DEBUG:
            self.print_p(p)
        p[0] = float(p[1])
        
    def p_number_2(self, p):
        "number : int exp"
        if DEBUG:
            self.print_p(p)
        p[0] = float("{0}{1}".format(p[1], p[2]))
        
    def p_number_3(self, p):
        "number : float exp"
        if DEBUG:
            self.print_p(p)
        p[0] = float("{0}{1}".format(p[1], p[2]))
        
    def p_int_0(self, p):
        "int : MINUS int"
        if DEBUG:
            self.print_p(p)
        p[0] = -p[2]
        
    def p_int_1(self, p):
        "int : NUMBER"
        if DEBUG:
            self.print_p(p)
        p[0] = p[1]
        
    def p_float_0(self, p):
        "float : MINUS float"
        p[0] = p[2] * -1
        
    def p_float_1(self, p):
        "float : FLOAT"
        p[0] = p[1]
        
    def p_exp_0(self, p):
        "exp : EPLUS NUMBER"
        if DEBUG:
            self.print_p(p)
        p[0] = "e{0}".format(p[2])

    def p_exp_1(self, p):
        "exp : EMINUS NUMBER"
        if DEBUG:
            self.print_p(p)
        p[0] = "e-{0}".format(p[2])
   
    
    # useful for debugging the parser
    def print_p(self, p):
        if DEBUG:
            name = inspect.getouterframes(inspect.currentframe(), 2)[1][3]
            print('(%d) %20s: %s' % (p.lexer.lex.lineno-1, name, ' | '.join([str(p[i]) for i in range(0, len(p))])))

    def p_error(self, p):
        # Derived from https://groups.google.com/forum/#!topic/ply-hack/spqwuM1Q6gM
        
        #Ugly hack since Ply doesn't provide any useful error information
        try:
            frame = inspect.currentframe()
            cvars = frame.f_back.f_locals
            expected = "; expected %s" % (', '.join(cvars['actions'][cvars['state']].keys()))
        except:
            expected = ""
        
        if p is not None:
            msg = "Line %d, column %d: unexpected %s%s" % (p.lineno, p.lexpos, p.type, expected)
        else:
            msg = "Unexpected end of file%s" % expected
        
        raise ValueError(msg)
    
        
    def __init__(self):
        self.yacc = yacc.yacc(module=self, debug=False, optimize=1, picklefile=pickle_file)
        self.block_stack = [] # A list of Blocks
        self.working_block_stack = [] # For calculations
        self.parent_block_indices = [] # Line numbers that the parent Blocks are defined on
        
    def parse(self, s):
        start = time.time()
        # Convert each blank line to !NL!
        lines = s.split('\n')

        for i, line in enumerate(lines):
            if not line.strip():
                lines[i] = '!NL!'
        s = '\n'.join(lines)
        #print (s)

        self.lexer = Lexer()
        result = self.yacc.parse(s, self.lexer)

        self.post_parse()
        end = time.time()
        print ('Took', end - start)
        for block in self.block_stack:
            print block

        return result


    def post_parse(self):
        """Run a series of functions after the main parse() function
        has completed.
        """
        self.block_stack.sort(key=lambda x: x.line_start)

        self.hide_child_blocks()

        text = self.lexer.lex.lexdata.split('\n')
        self.convert_text_to_tokens(text)

        self.find_commented_lines()

        # TODO: do we need to convert the tabs to spaces beforehand?
        # TODO: empty lines after the final brace are missing.

    def hide_child_blocks(self):
        """Remove any inner blocks from the block stack; we don't want to expose
        them outside of their parents.
        """
        blocks = filter(lambda block: block.line_start in self.parent_block_indices, self.block_stack)
        self.block_stack = blocks

    def convert_text_to_tokens(self, text):
        """Convert the strings in a block identifier to Tokens.
        This gives us more information about the strings, which is useful
        for querying them.
        """
        for block in self.block_stack:
            strings = text[block.line_start].split()[:-1] # Exclude the trailing brace
            start = end = 0

            for s in strings:
                end = start + len(s)
                s = re.sub('"', '', s)
                block.add_token(Token(start, end, s))
                start = end + 1

    def find_commented_lines(self):
        """Assign a LineType to each of the 'commented' lines -- they
        were skipped by the lexer.
        """
        self.comments = [(line_no, LineType.IGNORE) for line_no in self.lexer.commented_lines]
        print ('comments', self.comments)
        # TODO: the problem with this approach is that there are now two lists to
        # search: one for Blocks and one for comments.

class Block(object):
    def __init__(self, line_start=-1, line_end=-1, keyword=None, type=None, name=None):
        self.line_start = line_start # The line number of the block's definition
        self.line_end = line_end
        self.keyword = keyword
        self.type = type
        self.name = name
        self.arguments = [] # (line_no, LineType, object)

    def __repr__(self):
        args = ''
        for arg in self.arguments:
            args += ('\t%d => %s / %s' % (arg[0], arg[1], arg[2]))

        str = ('''BLOCK => ( Line start: %s Line end: %s Keyword: %s Type: %s Name: %s Arguments: %s )'''
               % (self.line_start, self.line_end, self.keyword, self.type, self.name, args))

        return str

    def add_block_identifier(self, string):
        """Assign the string as the keyword first, then the type, as we always
        have a keyword, but not always a type.
        """
        if not self.keyword:
            self.keyword = string
        elif not self.name:
            self.name = string
        else:
            if not self.type:
                self.type = self.name
                self.name = string
            else:
                print ('ERROR: block cannot have more than 3 block IDs. Skipping...') # TODO: need error function

    def add_token(self, token):
        """Replace a block identifier with its Token equivalent.
        """
        for attr in ['keyword', 'name', 'type']:
            block_id = object.__getattribute__(self, attr)
            if block_id is not Token and block_id == token.text:
                object.__setattr__(self, attr, token)
                return
        else:
            print ('ERROR: token %s does not match any block identifier.' % token )

    def add_argument(self, line_no, line_type, argument):
        """An argument can be either a key/value pair or a Block.
        """
        self.arguments.append((line_no, line_type, argument))

    def get_argument_names(self):
        """Return the argument names that have been added to this block.
        """
        args = []

        for argument in self.arguments:
            _, line_type, arg = argument
            if type(arg) is Block:
                args.append(arg.keyword)
            elif type(arg) is dict:
                return list(arg.keys())[0]

class Token():
    """A Token represents a string in a Terraform file.
    """
    def __init__(self, start, end, text):
        self.start = start
        self.end = end
        self.text = text

    def __repr__(self):
        return ('Token => ( Start:%s End:%s Text:%s )' % (self.start, self.end, self.text))

class LineType(Enum):
    BLOCK_ARGUMENT = 1
    BLOCK_EMPTY_LINE = 2
    BLOCK_DEFINITION = 3
    BLOCK_END = 4
    IGNORE = 5

